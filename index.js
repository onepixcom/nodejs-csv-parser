const cluster = require('cluster');
const os = require('os');
const fs = require("fs");

const puppeteer = require("puppeteer");
const csv = require("@fast-csv/parse");

function readCsv(path, options, rowProcessor) {
    return new Promise((resolve, reject) => {
        const data = [];

        csv
            .parseFile(path, options)
            .on("error", reject)
            .on("data", (row) => {
                const obj = rowProcessor(row);
                if (obj) data.push(obj);
            })
            .on("end", () => {
                resolve(data);
            });
    });
}

(async () => {
    if(cluster.isMaster) {
        console.log(`Master started. Pid: ${process.pid}`);

        const data = await readCsv(
            "./data.csv",
            { skipRows: 1 },
            (row) => row,
        );

        const cpusCount = Math.min(os.cpus().length - 1, data.length);

        for (let i = 0; i < cpusCount; i++) {
            const worker = cluster.fork();

            worker.on('exit', () => {
                console.log(`Worker ${i + 1}/${cpusCount} finished work!`)
            })

            worker.send(JSON.stringify({
                data: data,
                from: Math.ceil(data.length / cpusCount * i),
                to: Math.ceil(data.length / cpusCount * (i+1)),
            }));
        }
    }

    if(cluster.isWorker) {
        console.log(`Worker started. Pid: ${process.pid}`);
        process.on('message', async (msg) => {
            msg = JSON.parse(msg);

            const browser = await puppeteer.launch({
                args: ['--no-sandbox'],
                headless: true
            });
            fs.mkdir(`./screens`, () => {});

            const page = await browser.newPage();
            await page.setViewport({ width: 1216, height: 900, deviceScaleFactor: 2 });

            for (let i = msg.from; i < msg.to; i++) {
                try {
                    const [url, fileName] = msg.data[i];
                    await page.goto(url);
                    await page.screenshot({ path: `./screens/${fileName}.png` });
                    console.log(`${fileName} ready`);
                } catch (e) {
                    console.error(e);
                    console.error(i);
                }
            }

            await browser.close();
            process.exit(0);
        })
    }
})()